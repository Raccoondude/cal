package com.example.cal2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    String num_one = "";
    String num_two = "";
    boolean bool = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView out = findViewById(R.id.TextView);
    }
    public void onee(View view) {
        TextView out = findViewById(R.id.TextView);
        if (bool == true) {
            num_one = num_one + '1';
            out.setText(num_one);
        } else {
            num_two = num_two + '1';
            out.setText(num_two);
        }
    }
    public void two(View view) {
        TextView out = findViewById(R.id.TextView);
        if (bool == true) {
            num_one = num_one + '2';
            out.setText(num_one);
        } else {
            num_two = num_two + '2';
            out.setText(num_two);
        }
    }
    public void three(View view) {
        TextView out = findViewById(R.id.TextView);
        if (bool == true) {
            num_one = num_one + '3';
            out.setText(num_one);
        } else {
            num_two = num_two + '3';
            out.setText(num_two);
        }
    }
    public void four(View view) {
        TextView out = findViewById(R.id.TextView);
        if (bool == true) {
            num_one = num_one + '4';
            out.setText(num_one);
        } else {
            num_two = num_two + '4';
            out.setText(num_two);
        }
    }
    public void five(View view) {
        TextView out = findViewById(R.id.TextView);
        if (bool == true) {
            num_one = num_one + '5';
            out.setText(num_one);
        } else {
            num_two = num_two + '5';
            out.setText(num_two);
        }
    }
    public void six(View view) {
        TextView out = findViewById(R.id.TextView);
        if (bool == true) {
            num_one = num_one + '6';
            out.setText(num_one);
        } else {
            num_two = num_two + '6';
            out.setText(num_two);
        }
    }
    public void seven(View view) {
        TextView out = findViewById(R.id.TextView);
        if (bool == true) {
            num_one = num_one + '7';
            out.setText(num_two);
        } else {
            num_two = num_two + '7';
            out.setText(num_two);
        }
    }
    public void eight(View view) {
        TextView out = findViewById(R.id.TextView);
        if (bool == true) {
            num_one = num_one + '8';
            out.setText(num_one);
        } else {
            num_two = num_two + '8';
            out.setText(num_two);
        }
    }
    public void nine(View view) {
        TextView out = findViewById(R.id.TextView);
        if (bool == true) {
            num_one = num_one + '9';
            out.setText(num_one);
        } else {
            num_two = num_two + '9';
            out.setText(num_two);
        }
    }
    public void zero(View view) {
        TextView out = findViewById(R.id.TextView);
        if (bool == true) {
            num_one = num_one + '0';
            out.setText(num_one);
        } else {
            num_two = num_two + '0';
            out.setText(num_two);
        }
    }
    public void add(View view) {
        TextView out = findViewById(R.id.TextView);
        if (bool == true) {
            bool = false;
        } else {
            bool = true;
        }
        out.setText("");
    }
    public void total(View view) {
        TextView out = findViewById(R.id.TextView);
        int Total =  Integer.parseInt(num_one) + Integer.parseInt(num_two);
        String str_total = Integer.toString(Total);
        out.setText(str_total);
        num_one = "";
        num_two = "";
    }
    public void credit(View view) {
        startActivity(new Intent(MainActivity.this, Credit.class));
    }
    public void clear(View view) {
        bool = true;
        num_one = "";
        num_two = "";
        TextView out = findViewById(R.id.TextView);
        out.setText("");
    }
    public void OwO(View view) {
        TextView out = findViewById(R.id.TextView);
        out.setText("OwO");
    }
}
